# SchlOSM - First Open Street Map tower defense game

Have you ever wanted to put your skills to the test and defend your favorite real life castle/fort? Now you will be given the chance with SchlOSM (de: "Schloss" = en: "Castle" + OSM).

![SchlOSM Icon](icon.png)

## Timeline

This is the preliminary timeline. Expect it to change in time.

### 1. Basic Architecture Phase

In this phase the goal is 

- to create a basic game with a grid like map, 
- able to set and upgrade towers, 
- protect against first NPC waves

### 2. Intermediate Architecture Phase

- create multiple tower varieties
- first tech tree
- multiple NPC varieties

### 3. Artistic Phase

Add sprites for all above mentioned entities.

The initial art style will follow a medieval theme with a realistic gameplay logic.

In the future the user will have the option to upload their own assets to create an unique gameplay experience.

### 4. Open Street Map Phase

- import castle maps from OSM
    - the goal is to create a parser where one can paint the correct areas
    - there will be some pre-processing, though manual labor is key to reduce the potential corruption ability
- implement an anti-tamper for OSM data
    - the main concern behind this feature is that users will corrupt the data to gain personal advances
    - the corruption of the data will then generate a more poor experience for all other OSM users
    - a possible solution is to use the tagging scheme suggested by [OSM-4D](https://wiki.openstreetmap.org/wiki/Category:OSM-4D), this introduces the `3dr:*=*` key-value combination. Any changes here won't impact the rest of the global users while proving a template to create complex structures.

### 5. OSM Lat/Long Phase

Allow arbitrary lat/long and size combinations for custom maps

### 6. OSM Perks Phase

- get in-game rewards for contributing to OSM
- collectibles and achievements

### 7. Mobile Phase

- play the game on mobile
- solve IRL quests to improve OSM and Wikidata
- play in AR to get a more intense experience

### 8. Financial Phase

- the game will be free-to-play on Fdroid and itchio with donation base policy (Pro version)
- the Pro version will come with a small fee on the PlayStore and Steam
- the game can be played free on the PlayStore with a handicap and play-to-win model watching ads

## Development 

The game will be developed in [Rust](https://www.rust-lang.org/) using the [bevy engine](https://bevyengine.org/).

## Game

The game can be compiled using this repository or accessed on [itchio](https://barefootstache.itch.io/schlosm).

## Changelog

### 0.0 release

- game description
- game itchio website
